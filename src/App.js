import React, { Component, Fragment } from 'react';
import Nav from "./nav";
// import About from "./Components/countryview";
// import Shop from "./Components/Shop";
import {BrowserRouter as Router, Routes, Switch, Route} from "react-router-dom";
import './App.css';
import Home from "./Components/Home";
import CountryView from './Components/countryview';

class App extends React.Component {
  render() { 
    
    return (
      <Router>
      <div className="App">
        {/* <Nav/> */}
        <Switch>
    <Route exact path="/" ><Home></Home></Route>

        <Route exact path="/:id" render={(props)=><CountryView {...props}/>} />
   
       
       </Switch>
      </div>
      </Router>
     
    );
    
  }
}

export default App;
