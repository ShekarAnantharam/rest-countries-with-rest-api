import React, { Component } from 'react';
import * as API from "./API/api"
import Countries from './Countries'
class Home extends React.Component {

    constructor(props){
      super(props)
      this.state={
          data:[],
          searchData:[]
      }
  }
  getData=async ()=>{
    const data = await API.getData();
    this.setState({data:data, searchData:data})
    

  }
  handleSearch=(value)=>{
  let searchData=this.state.data.filter(country => {
    if (value==""){
      
      return country
    }
    else if(country.name.common.toLowerCase().includes(value.toLowerCase())){
      return country
    }
  
  }) 
  this.setState({searchData})
  }
  handleFilter=(value)=>{
    let searchData=this.state.data.filter(country => {
      if (value=="Filter by Region"){
        return country
      }
      else if(country.region.toLowerCase()==value.toLowerCase()){
        return country
      }
    
    }) 
    this.setState({searchData})
    }
  
  componentDidMount(){
    console.log("shekar")
    this.getData();
    // console.log(searchData);

  }
  componentDidUpdate(prevProps){
console.log(prevProps,'ht')
    // console.log(this.props.match.params.id,'namaste')
    // if(this.props.match.params.id!==prevProps.match.params.id){
      // this.getCountry()}
  
    }
    render() { 
     
      return (
   
  
          <div >
          <Countries countries = {this.state.searchData} 
              onSearch={this.handleSearch} onFilter={this.handleFilter}/> 
          
          </div>
  
    
      );
    }
  }
   
  export default Home;
  