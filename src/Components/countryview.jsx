import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

import "./styles.css";

class CountryView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      country: [1, 2, 3]
    };
  }
  async getCountry() {
    let id = this.props.match.params.id;
    const data = await (await fetch(
      `https://restcountries.com/v3.1/alpha/${id}`
    )).json();
    this.setState({ country: [...data] });
  }
  componentDidMount() {
    //    this.setState({id:this.props.match.params.id})
    this.getCountry();

    // console.log("countryDATA",this.state.country)
  }
  // async componentDidUpdate(){
  //     let {id}=this.props.match.params;

  //     const data= await axios.get(`https://restcountries.com/v3.1/name/${id}`)
  //     .then(res=>this.setState({country:res.data}))
  // }
  componentDidUpdate(prevProps, prevState) {
    console.log("update", prevProps);
    console.log("update", prevState);

    if (this.props.match.params.id !== prevProps.match.params.id) {
      // this.setState({id:pr/evProps.match.params.id})
      this.getCountry();
    }
    // let {id}=this.props.match.params;

    // const data= await (await fetch(`https://restcountries.com/v3.1/name/${id}`)).json()
    // this.setState({country:[...data]})
  }
  render() {
    return (
      <div className="country-container">
        <button className="back-button" onClick={this.props.history.goBack}>
          Back
        </button>
        {/* <img className="flag" src={this.state.country.} alt="" /> */}
        {/* <h1>this is country  view page</h1> */}
        {console.log("after rendered", this.state.country)}
        {this.state.country.map(item => {
          

          if (item.cca3 == this.props.match.params.id) {
            return ( 
              <div className="country">
                <div>
                  <img src={item.flags.png} alt="" />
                </div>
                <div className="shekar">
                  <p>
                    <span className="heading">{item.name.common}</span>
                  </p>
                  <div className="details-section">
                    <div>
                      <p>
                        <span className="heading">Native Name:</span>
                        {item.altSpellings[1]}
                      </p>
                      <p>
                        <span className="heading">Population:</span>
                        {item.population}
                      </p>
                      <p>
                        <span className="heading">Region:</span>
                        {item.region}
                      </p>
                      <p>
                        <span className="heading">Sub Region:</span>
                        {item.subregion}
                      </p>
                      <p>
                        <span className="heading">Capital:</span>
                        {item.capital}
                      </p>
                    </div>
                    <div>
                      <p>
                        <span className="heading">Top Level Domain:</span>
                        {item.tld}
                      </p>
                      <p>
                        <span className="heading">Currencies:</span>
                        
                        {Object.values(item.currencies).map(c => {
                          return c.name;
                        })}
                      </p>
                      <p>
                        <span className="heading">Languages:</span>
                        {Object.values(item.languages).map(lan => {
                          return <span>{lan},</span>;
                        })}
                      </p>
                    </div>
                  </div>
                  <div className="border-countries">
                  

                    <span className="heading"> Border Countries:</span>
                    <div className="border-countries-div">
                    
                      { 
                          item.borders.map((country, index) => {
                         if(item.hasOwnProperty("borders")){
                            return (
                                <button className="border-country-button">
                                  <Link className="link" to={`/${country}`}>
                                    {country}
                                  </Link>
                                </button>
                              );
                         }
                         else{
                             return;
                         }
                            
                          
                        
                      })}
                    </div>

                  </div>
                </div>
              </div>
            );
          }
        })}
      </div>
    );
  }
}

export default CountryView;
