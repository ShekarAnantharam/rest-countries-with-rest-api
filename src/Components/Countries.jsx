import React, { Component } from "react";
import "./styles.css";
import { Link } from "react-router-dom";
class Countries extends React.Component {
  // {console.log('hi')}

  render() {
    const divStyle={
      // background:"white",
      width: "22vw",
      
    }
    console.log("searchdata",this.props)
    return (
      <div>
        <div className="search-filter">
          <input
            type="text"
            placeholder="Search for Country..."
            onChange={event => {
              this.props.onSearch(event.target.value);
            }}
          />
          <select
            onChange={event => {
              this.props.onFilter(event.target.value);
            }}
          >
            <option value="Filter by Region">Filter by Region</option>
            <option value="Africa">Africa</option>
            <option value="Asia">Asia</option>
            <option value="Americas">Americas</option>
            <option value="Europe">Europe</option>
            <option value="Oceania">Oceania</option>
          </select>
        </div>
        <div className="countries">
          {this.props.countries.map((country, index) => {
            return (
              
              <div key={index} className="single">
              {/* <Link style={divStyle} to="/name" > */}
              <Link className="link" to={`/${country.cca3}`}>
                <div className="image">
                  <img className="flag" src={country.flags.png} alt="" />
                </div>
                <div className="country-details">
                  <p className="name">{country.name.common}</p>
                  <p className="other-details">
                    <span className="denotion">Population:</span>
                    {country.population}
                  </p>

                  <p className="other-details">
                    <span className="denotion">Region:</span>
                    {country.region}
                  </p>
                  <p className="other-details">
                    <span className="denotion">Capital:</span>
                    {country.capital}
                  </p>
                  {/* {console.log(country)} */}
                </div>
                </Link>
              {/* </Link> */}

              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default Countries;
